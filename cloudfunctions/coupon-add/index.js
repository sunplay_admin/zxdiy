// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: 'product-6d4b5e'
})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  try {
    if (event.t == 0) {
      return await db.collection('coupon_info').add({
        // data 字段表示需新增的 JSON 数据
        data: {
          diyId:event.diyId,
          yhqname: event.yhqname,
          starttime: event.starttime,
          endtime: event.endtime,
          starttimet: event.starttimeT,
          endtimet: event.endtimeT,
          num: event.ffsl,
          limit_num: event.mrxl,
          condition: event.sytj,
          price: event.yhje,
          state: event.state,
          has_num: event.ffsl,
          has_received: 0,
          length: String(event.yhje).length,
          createtime: event.createtime
        }
      })
    } else if(event.t==1) {
      return await db.collection('coupon_info').doc(event.id).update({
        // data 字段表示需新增的 JSON 数据
        data: {
          yhqname: event.yhqname,
          starttime: event.starttime,
          endtime: event.endtime,
          starttimet: event.starttimeT,
          endtimet: event.endtimeT,
          num: event.ffsl,
          limit_num: event.mrxl,
          condition: event.sytj,
          price: event.yhje,
          state: event.state,
          has_num: event.ffsl,
          has_received: 0,
          length: String(event.yhje).length,
          createtime: event.createtime}
      })
    }else if(event.t==2){
      const user_cou = await db.collection('user_coupon').where({
          openid: event.openid, // 填入当前用户 openid
          yhqid:event.id,
          diyId: event.diyId
        }).get();
      const pro = await db.collection('coupon_info').doc(event.id).get();
      if (user_cou.data.length == pro.data.limit_num){
        return { code: 202, message: '您已经抢过券啦' };
      }
      if(pro.data.has_num<=0){
        return {code:201,message:'该优惠券已经被抢完'};
      }else{
          await db.collection('user_coupon').add({
            // data 字段表示需新增的 JSON 数据
            data: {
              diyId:event.diyId,
              yhqid: event.id,
              openid: event.openid,
              state: 0,  //0未使用 1已使用
              yhqname:event.yhqname,
              num:event.num,
              limit_num:event.limit_num,
              price:event.price,
              condition:event.condition,
              starttime:event.starttime,
              starttimet:event.starttimet,
              endtime: event.endtime,
              endtimet: event.endtimet,
              is_select:false,
              length: String(event.price).length,
              createtime: event.createtime
            }
          });
        
        return await db.collection('coupon_info').doc(event.id).update({
          // data 字段表示需新增的 JSON 数据
          data: {
            has_num: pro.data.has_num - 1,
            has_received: pro.data.has_received+1
          }
        })
      }
      
    }
  } catch (e) {
    console.error(e)
  }
}